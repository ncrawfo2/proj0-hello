# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms. Prints a message specified in credentials.ini, in this
case "Hello World."

Author: Nathan Crawford
Contact: ncrawfo2@uoregon.edu
